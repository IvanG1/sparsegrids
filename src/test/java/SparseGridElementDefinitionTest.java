import Exceptions.DimensionIdOutOfBounds;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class SparseGridElementDefinitionTest {
    private SparseGridElementDefinition sparseGridElementDefinition;

    @BeforeEach
    void setUp() {
        ArrayList<Integer> levels = new ArrayList<>(Arrays.asList(1, 2, 1));
        sparseGridElementDefinition = new SparseGridElementDefinition(levels);
    }

    @Test
    void getDimension_returns_nb_of_levels() {
        assertEquals(3, sparseGridElementDefinition.getDimension());
    }

    @Test
    void getLevel_allows_to_retrieve_level_from_dimension_id() {
        assertEquals(2, sparseGridElementDefinition.getLevel(1));
    }

    @Test()
    void getLevel_throws_exception_if_dimension_id_exceeds_element_dimension() {
        Assertions.assertThrows(DimensionIdOutOfBounds.class, () -> sparseGridElementDefinition.getLevel(3));
    }

    @Test
    void equality() {
        ArrayList<Integer> sameLevels = new ArrayList<>(Arrays.asList(1, 2, 1));
        SparseGridElementDefinition sameSparseGridElementDefinition = new SparseGridElementDefinition(sameLevels);
        assertEquals(sparseGridElementDefinition, sameSparseGridElementDefinition);
    }

    @Test
    void non_equality() {
        ArrayList<Integer> sameLevels = new ArrayList<>(Arrays.asList(2, 2, 1));
        SparseGridElementDefinition sameSparseGridElementDefinition = new SparseGridElementDefinition(sameLevels);
        assertNotEquals(sparseGridElementDefinition, sameSparseGridElementDefinition);
    }
}