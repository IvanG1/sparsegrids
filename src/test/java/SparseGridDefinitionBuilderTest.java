import Exceptions.DimensionDoesNotMatch;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SparseGridDefinitionBuilderTest {
    @Test
    void build_empty_grid() {
        assertEquals(new SparseGridDefinition(new ArrayList<>()),
                SparseGridDefinitionBuilder.aSparseGridDefinitionBuilder(3).build());
    }

    @Test
    void build_grid_with_single_default_element() {
        SparseGridDefinition expectedGrid = new SparseGridDefinition(new ArrayList<>(Collections.singletonList(
                new SparseGridElementDefinition(new ArrayList<>(Collections.nCopies(3, 1))))));

        assertEquals(expectedGrid, SparseGridDefinitionBuilder.aSparseGridDefinitionBuilder(3)
                .withGridElement(SparseGridElementDefinitionBuilder.aSparseGridElementDefinition(3))
                .build());
    }

    @Test
    void build_grid_with_single_element_with_dimension_at_second_index() {
        SparseGridDefinition expectedGrid = new SparseGridDefinition(new ArrayList<>(Collections.singletonList(
                new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(1, 5))))));

        assertEquals(expectedGrid, SparseGridDefinitionBuilder.aSparseGridDefinitionBuilder(2)
                .withGridElement(SparseGridElementDefinitionBuilder.aSparseGridElementDefinition(2).withDimensionLevel(1, 5))
                .build());
    }

    @Test
    void throws_if_append_grid_element_of_dimension_other_than_that_of_the_grid_builder() {
        Assertions.assertThrows(DimensionDoesNotMatch.class, () ->
                SparseGridDefinitionBuilder.aSparseGridDefinitionBuilder(3).withGridElement(SparseGridElementDefinitionBuilder.aSparseGridElementDefinition(5)).build());
    }

    @Test
    void acceptance_test() {
        SparseGridDefinition expectedGrid = new SparseGridDefinition(new ArrayList<>(Arrays.asList(
                new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(2, 1, 1))),
                new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(1, 2, 1))),
                new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(1, 1, 2))))));

        assertEquals(expectedGrid, SparseGridDefinitionBuilder.aSparseGridDefinitionBuilder(3)
                .withGridElement(SparseGridElementDefinitionBuilder.aSparseGridElementDefinition(3).withDimensionLevel(0, 2))
                .withGridElement(SparseGridElementDefinitionBuilder.aSparseGridElementDefinition(3).withDimensionLevel(1, 2))
                .withGridElement(SparseGridElementDefinitionBuilder.aSparseGridElementDefinition(3).withDimensionLevel(2, 2))
                .build());
    }
}