import Exceptions.DimensionIdOutOfBounds;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SparseGridElementDefinitionBuilderTest {
    @Test
    void build_default_element_of_size_five() {
        assertEquals(new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(1, 1, 1, 1, 1))),
                SparseGridElementDefinitionBuilder.aSparseGridElementDefinition(5).build());
    }

    @Test
    void build_element_of_size_four_and_level_three_at_first_dimension() {
        assertEquals(new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(3, 1, 1, 1))),
                SparseGridElementDefinitionBuilder.aSparseGridElementDefinition(4).withDimensionLevel(0,3).build());
    }

    @Test
    void throws_if_dimension_id_is_out_of_bounds() {
        Assertions.assertThrows(DimensionIdOutOfBounds.class, () ->
                SparseGridElementDefinitionBuilder.aSparseGridElementDefinition(7).withDimensionLevel(10, 5));
    }
}