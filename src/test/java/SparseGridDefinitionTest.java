import Exceptions.ElementIdOutOfBounds;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class SparseGridDefinitionTest {
    private SparseGridDefinition sparseGridDefinition;

    @BeforeEach
    void setUp() {
        SparseGridElementDefinition e1 = new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(2, 1, 1)));
        SparseGridElementDefinition e2 = new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(1, 2, 1)));
        SparseGridElementDefinition e3 = new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(1, 1, 3)));

        ArrayList<SparseGridElementDefinition> elements = new ArrayList<>(Arrays.asList(e1, e2, e3));
        sparseGridDefinition = new SparseGridDefinition(elements);
    }

    @Test
    void return_nb_of_elements() {
        assertEquals(3, sparseGridDefinition.getNumberOfElements());
    }

    @Test
    void return_element_by_element_id() {
        SparseGridElementDefinition expectedElement = new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(1, 2, 1)));
        assertEquals(expectedElement, sparseGridDefinition.getElement(1));
    }

    @Test()
    void getElement_throws_exception_if_dimension_id_exceeds_element_dimension() {
        Assertions.assertThrows(ElementIdOutOfBounds.class, () -> sparseGridDefinition.getElement(3));
    }

    @Test
    void equality(){
        SparseGridElementDefinition e1 = new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(2, 1, 1)));
        SparseGridElementDefinition e2 = new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(1, 2, 1)));
        SparseGridElementDefinition e3 = new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(1, 1, 3)));

        ArrayList<SparseGridElementDefinition> sameElements = new ArrayList<>(Arrays.asList(e1, e2, e3));
        SparseGridDefinition sameSparseGridDefinition = new SparseGridDefinition(sameElements);
        assertEquals(sameSparseGridDefinition, sparseGridDefinition);
    }

    @Test
    void equality_when_reordered(){
        SparseGridElementDefinition e1 = new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(2, 1, 1)));
        SparseGridElementDefinition e2 = new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(1, 2, 1)));
        SparseGridElementDefinition e3 = new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(1, 1, 3)));

        ArrayList<SparseGridElementDefinition> sameElements = new ArrayList<>(Arrays.asList(e3, e1, e2));
        SparseGridDefinition sameExhaustiveSparseGridDefinition = new SparseGridDefinition(sameElements);
        assertEquals(sameExhaustiveSparseGridDefinition, sparseGridDefinition);
    }

    @Test
    void non_equality(){
        SparseGridElementDefinition e1 = new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(3, 1, 1)));
        SparseGridElementDefinition e2 = new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(1, 2, 1)));
        SparseGridElementDefinition e3 = new SparseGridElementDefinition(new ArrayList<>(Arrays.asList(1, 1, 2)));

        ArrayList<SparseGridElementDefinition> sameElements = new ArrayList<>(Arrays.asList(e1, e2, e3));
        SparseGridDefinition sameExhaustiveSparseGridDefinition = new SparseGridDefinition(sameElements);
        assertNotEquals(sameExhaustiveSparseGridDefinition, sparseGridDefinition);
    }
}