import Exceptions.DimensionIdOutOfBounds;

import java.util.ArrayList;
import java.util.Objects;

public class SparseGridElementDefinition {
    private ArrayList<Integer> levels;

    SparseGridElementDefinition(ArrayList<Integer> levels) {
        this.levels = levels;
    }

    int getDimension(){
        return levels.size();
    }

    int getLevel(int dimensionIndex){
        try{
            return levels.get(dimensionIndex);
        }
        catch(IndexOutOfBoundsException e)
        {
            throw new DimensionIdOutOfBounds();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SparseGridElementDefinition that = (SparseGridElementDefinition) o;
        return Objects.equals(levels, that.levels);
    }

    @Override
    public int hashCode() {
        return Objects.hash(levels);
    }

    @Override
    public String toString() {
        return "SparseGridElementDefinition{" +
                levels +
                '}';
    }

    public static class Comparator implements java.util.Comparator<SparseGridElementDefinition> {
        public int compare(SparseGridElementDefinition first, SparseGridElementDefinition second) {
            return first.levels.toString().compareTo(second.levels.toString());
        }
    }
}

