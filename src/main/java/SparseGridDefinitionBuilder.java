import Exceptions.DimensionDoesNotMatch;

import java.util.ArrayList;

class SparseGridDefinitionBuilder {
    private int dimension;
    private ArrayList<SparseGridElementDefinition> elements = new ArrayList<>();

    private SparseGridDefinitionBuilder(int dimension) {
        this.dimension = dimension;
    }

    static SparseGridDefinitionBuilder aSparseGridDefinitionBuilder(int dimension){
        return new SparseGridDefinitionBuilder(dimension);
    }

    SparseGridDefinition build() {
        return new SparseGridDefinition(elements);
    }

    private void addGridElement(SparseGridElementDefinitionBuilder elementBuilder) {
        SparseGridElementDefinition element = elementBuilder.build();

        if (element.getDimension() != dimension){
            throw new DimensionDoesNotMatch();
        }

        elements.add(elementBuilder.build());
    }

    SparseGridDefinitionBuilder withGridElement(SparseGridElementDefinitionBuilder aSparseGridElementDefinition) {
        addGridElement(aSparseGridElementDefinition);
        return this;
    }
}
