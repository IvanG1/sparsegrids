import Exceptions.ElementIdOutOfBounds;

import java.util.ArrayList;
import java.util.Objects;

public class SparseGridDefinition {
    private ArrayList<SparseGridElementDefinition> elements;

    SparseGridDefinition(ArrayList<SparseGridElementDefinition> elements) {
        this.elements = elements;
        this.elements.sort(new SparseGridElementDefinition.Comparator());
    }

    int getNumberOfElements(){
        return elements.size();
    }

    SparseGridElementDefinition getElement(int elementId){
        try{
            return elements.get(elementId);
        }
        catch(IndexOutOfBoundsException e)
        {
            throw new ElementIdOutOfBounds();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SparseGridDefinition that = (SparseGridDefinition) o;
        return Objects.equals(elements, that.elements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(elements);
    }

    @Override
    public String toString() {
        return "SparseGridDefinition{" +
                elements +
                '}';
    }
}
