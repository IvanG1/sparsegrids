import Exceptions.DimensionIdOutOfBounds;

import java.util.ArrayList;
import java.util.Collections;

class SparseGridElementDefinitionBuilder {
    private ArrayList<Integer> levels;

    static SparseGridElementDefinitionBuilder aSparseGridElementDefinition(int dimension){
        return new SparseGridElementDefinitionBuilder(dimension);
    }

    private SparseGridElementDefinitionBuilder(int dimension) {
        levels = new ArrayList<>(Collections.nCopies(dimension, 1));
    }

    SparseGridElementDefinition build() {
        return new SparseGridElementDefinition(levels);
    }

    private void setDimensionLevel(int dimensionIndex, int level) {
        try {
            levels.set(dimensionIndex, level);
        } catch(IndexOutOfBoundsException e)
        {
            throw new DimensionIdOutOfBounds();
        }
    }

    SparseGridElementDefinitionBuilder withDimensionLevel(int dimensionIndex, int level) {
        setDimensionLevel(dimensionIndex, level);
        return this;
    }
}
